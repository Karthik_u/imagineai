from Global_imports import flask,flask_login,Flask,render_template,CORS,os,request,secure_filename,Main,xedcircle,OCR_tess,Img_enhance


app = Flask(__name__)
app.secret_key = 'sanriaICDS'
app.config['UPLOAD_FOLDER'] = r'./static/ICDS/demo01/input'
CORS(app)
login_manager = flask_login.LoginManager()
login_manager.init_app(app)
imgPath = ''
img125xPath = ''

BASE_DIR = r'./'

if os.path.exists(os.path.join(BASE_DIR, '/static/ICDS/')):
    pass
else:
    # os.mkdir(r'./static/ICDS/')
    pass

users = {'demo@virtuele.us': {'password': '12345'}}


class User(flask_login.UserMixin):
    pass


@login_manager.user_loader
def user_loader(email):
    if email not in users:
        return
    user = User()
    user.id = email
    return user


@login_manager.request_loader
def request_loader(request):
    email = request.form.get('email')
    if email not in users:
        return
    user = User()
    user.id = email
    user.is_authenticated = request.form['password'] == users[email]['password']
    return user


@app.route('/', methods=['GET', 'POST'])
def login():
    if flask.request.method == 'GET':
        return render_template("login.html")

    email = flask.request.form['email']
    if flask.request.form['password'] == users[email]['password']:
        user = User()
        user.id = email
        flask_login.login_user(user)
        return flask.redirect(flask.url_for('index'))

    return 'Bad login'


# @app.route('/protected')
# @flask_login.login_required
# def protected():
#     return 'Logged in as: ' + flask_login.current_user.id


@app.route('/logout')
def logout():
    flask_login.logout_user()
    return 'Logged out'


@login_manager.unauthorized_handler
def unauthorized_handler():
    return 'Unauthorized'


@app.route('/home')
def index():
    return render_template("test_base.html")


@app.route('/viewImage')
def showImage():
    #imgurl= 'http://127.0.0.1:5000/' + imgPath
    print(imgPath)
    return render_template('test_showimage.html', imageurl=imgPath)

@app.route('/downloadResultJson')
def jsonDownload():
    return render_template('test_resultjson.html')


@app.route('/viewStickDiagram')
def stickDiagram():
    return render_template('test_stickdiagram.html')


@app.route('/help')
def help():
    return render_template('test_help.html')


@app.route('/imgUpload', methods=['POST'])
def uploadImg():
     if request.method == 'POST':
        f = request.files['file']
        global imgPath,img125xPath
        imgName = secure_filename(f.filename)
        imgNameWOExt=imgName.split('.',2)[0]
        imgPath = os.path.join(app.config['UPLOAD_FOLDER'], imgName)
        print('img125xPath',img125xPath)
        f.save(os.path.join(app.config['UPLOAD_FOLDER'], imgName))
        img125xPath = Img_enhance.imageEnhance('demo01', imgName)
        return img125xPath

    # if request.method == 'POST':
    #     f = request.files['file']
    #     global imgPath,img125xPath
    #     imgName = secure_filename(f.filename)
    #     imgNameWOExt=imgName.split('.',2)[0]
    #     imgPath = os.path.join(app.config['UPLOAD_FOLDER'], imgName)
    #     img125xPath = os.path.join(app.config['UPLOAD_FOLDER'], '125x_'+imgNameWOExt+'.png')
    #     print('img125xPath',img125xPath)
    #     f.save(os.path.join(app.config['UPLOAD_FOLDER'], imgName))
    #     return img125xPath


# Grid end-points

Grid_DetectedURl=''
@app.route('/detectgrids', methods=['POST'])
def detectgrids():
    global Grid_DetectedURl
    Grid_DetectedURl=xedcircle.detect_grids('demo01',img125xPath )
    return 'Grid Detection Complete'

@app.route('/viewGrids')
def showGrids():
    return render_template('test_showgrids.html', gridImageurl=Grid_DetectedURl)

@app.route('/getGrids')
def getgrids():
    print(imgPath)
    return render_template('test_getgrids.html', imageurl=img125xPath)


@app.route('/setGridRadius', methods=['POST'])
def getCrped():
    clicked=None
    global Grid_DetectedURl
    #if request.method == "POST":
    clicked=request.json['data']
    print(img125xPath)
    Grid_DetectedURl=OCR_tess.getGridManually('demo01',img125xPath,clicked)
    return render_template('test_showgrids.html', gridImageurl=Grid_DetectedURl)

# Profile end-points
@app.route('/detectprofiles', methods=['POST'])
def detectprofiles():
    #json 
    Main.detectProfiles('demo01',img125xPath)
    return 'detectProfiles-Complete'

@app.route('/putEASTJSON', methods=['POST'])
def puteastjson():
    EASTJson=request.json['data']
    
    return 'detectProfiles-Complete'

if __name__ == "__main__":
    app.run(debug=True)
