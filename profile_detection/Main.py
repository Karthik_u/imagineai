import Global_imports as GImport
from profile_detection import marktext
input_path = r'./ICDS/demo01/input/'


def detectProfiles(id, file):
    filsplit = file.split('/', 5)
    imgFileName = filsplit[len(filsplit)-1]
    imgFileNameExt = imgFileName.split('.', 2)[0]
    imgFileNameExt = imgFileNameExt[5:]
    print('file name', imgFileNameExt)
    path = file
    
    print('eval started')
    marktext.eval(id,path)

    image = GImport.cv2.imread(path)
    image1 = image
    img_gray = GImport.cv2.cvtColor(image, GImport.cv2.COLOR_BGR2GRAY)

    jsfile = r'./static/ICDS/'+id+'/Profile_detection/input_json/output4_th.json'
    outdir = r'./static/ICDS/'+id+'/Profile_detection/cropped_profiles/'
    GImport.east_crop.crop_east_ims(jsfile, input_path, path, outdir)

    Cropped_profiles_input_path = r'./static/ICDS/' + \
        id+'/Profile_detection/cropped_profiles/'
    Processed_profiles_input_path = r'./static/ICDS/' + \
        id+'/Profile_detection/processed_profiles/'
    TextOutDir = r'./static/ICDS/'+id+'/Profile_detection/text_output/'
    XlsOutDir = r'./static/ICDS/'+id+'/Profile_detection/xlsoutput/'

    files = GImport.os.listdir(Cropped_profiles_input_path)
    OCR_Output = []
    for file in files:
        if GImport.os.path.isfile(Cropped_profiles_input_path + file):
            OCR_Text = GImport.OCR_tessProfile.myOCRforSlices(
                Cropped_profiles_input_path+file, Processed_profiles_input_path, 0)
            OCR_Output.append(OCR_Text)

    OCR_Txt_File = TextOutDir+imgFileNameExt+'.txt'

    xlsoutfile = XlsOutDir+imgFileNameExt+'_Profiles.xlsx'

    # create of file xlsx using workbook class
    workbook = GImport.xlsxwriter.Workbook(xlsoutfile)

    # By default worksheet names in the spreadsheet will be
    # Sheet1, Sheet2 etc., but we can also specify a name.
    worksheet = workbook.add_worksheet("Profile_Coordinates")
    profilewithcoordinate = [
        ['Profile text', 'X-min', 'Y-min', 'X-max', 'Y-max']]
    for i in range(len(OCR_Output)):
        codict = OCR_Output[i]
        profilecoorddinate = codict['Co-ordinates']
        profilecoorddinateArr = []
        profilecoorddinateArr = profilecoorddinate.split('_', 5)
        profiletext = codict['Text']
        data = [profiletext, profilecoorddinateArr[1], profilecoorddinateArr[2],
                profilecoorddinateArr[3], profilecoorddinateArr[4]]
        profilewithcoordinate.append(data)

    profilewithcoordinatetuple = tuple(profilewithcoordinate)
    print(profilewithcoordinatetuple)
    # Start from the first cell. Rows and
    # columns are zero indexed.
    row = 0
    col = 0

    # Iterate over the data and write it out row by row.
    for gridName, xmin, ymin, xmax, ymax in (profilewithcoordinatetuple):
        worksheet.write(row, col, gridName)
        worksheet.write(row, col + 1, xmin)
        worksheet.write(row, col + 2, ymin)
        worksheet.write(row, col + 3, xmax)
        worksheet.write(row, col + 4, ymax)
        row += 1

    workbook.close()

    f = open(OCR_Txt_File, 'w+')
    try:
        for ind in range(len(OCR_Output)):
            f.write(str(OCR_Output[ind])+'\n')
    except:
        print('I/O Error')
    finally:
        f.close()
