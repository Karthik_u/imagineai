import pytesseract
import cv2
from PIL import Image
import imutils
import numpy as np
import os

def myOCR(img,out_prcssimg,rotation,txtfile):
    f = open(txtfile+".txt", "w+")

    print('file',img)
    img=cv2.imread(img)

    # Convert to gray
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Apply dilation and erosion to remove some noise
    kernel = np.ones((1, 1), np.uint8)
    img = cv2.dilate(img, kernel, iterations=1)
    img = cv2.erode(img, kernel, iterations=1)

    rotated90 = imutils.rotate_bound(img, rotation)
    img = rotated90

    proccessedimg=out_prcssimg+str(txtfile)+'_'+"removed_noise.png"
    # Write image after removed noise
    cv2.imwrite(proccessedimg, img)

    imgtoStr = pytesseract.image_to_string(Image.open(proccessedimg),lang='eng',config='--psm 6 -c tessedit_char_whitelist=[A-Z]')
    for i in imgtoStr.split():
        print(i)
        # f.write(str("Profiles"))
        f.write(str(i))
        f.write(str('\n'))
    #text
    print('text',imgtoStr)




def myOCRforSlices(img,out_prcssimg,rotation):
    #print('file',img)
    fname = os.path.splitext(img)[0]
    filename = fname.split('/', 2)

    img=cv2.imread(img)

    im_shape = img.shape
    if im_shape[0] >= im_shape[1]:
        rotated90 = imutils.rotate_bound(img, 270)
        img = rotated90

    # Convert to gray
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Apply dilation and erosion to remove some noise
    kernel = np.ones((1, 1), np.uint8)
    img = cv2.dilate(img, kernel, iterations=1)
    img = cv2.erode(img, kernel, iterations=1)

    rotated90 = imutils.rotate_bound(img, rotation)
    img = rotated90
    filesplit=filename[2].split('/',3)
    proccessedimg=out_prcssimg+filesplit[2]+'_'+"removed_noise.png"

    # Write image after removed noise
    cv2.imwrite(proccessedimg, img)
    imginfo = {}
    imginfo['Co-ordinates'] = str(filename[2])
    imgtoStr = pytesseract.image_to_string(Image.open(proccessedimg),lang='eng',config='--psm 6 -c tessedit_char_whitelist=[A-Z]')
    imginfo['Text']=imgtoStr

    #print('text',imginfo)
    return imginfo
