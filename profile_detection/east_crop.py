import Global_imports as GImport


def crop_east_ims(jfile,in_dir,filename,out_dir):

    jfile = GImport.json.load(open(jfile))
    jcoords = jfile['text_lines']
    in_dir = in_dir #r'C:\Users\Admin\Documents\Custom Office Templates\results\inp17'
    filename = filename #r'C:\Users\Admin\Documents\Custom Office Templates\results\inp17\input.png'
    out_dir = out_dir #r'C:\Users\Admin\Documents\Custom Office Templates\results\inp17\out'
    img_to_crop = GImport.Image.open(filename)
    
    for ind, coords in enumerate(jcoords):
        xcoords = [coords['x0'],coords['x1'],coords['x2'],coords['x3']]
        ycoords = [coords['y0'],coords['y1'],coords['y2'],coords['y3']]
        xmin = min(xcoords)
        ymin = min(ycoords)
        xmax = max(xcoords)
        ymax = max(ycoords)
        # print(in_dir)
        fname = GImport.os.path.basename(in_dir)+str(ind)+'_'+str(int(xmin))+'_'+str(int(ymin))+'_'+str(int(xmax))+'_'+str(int(ymax))+'.png'
        
        cropped_img = img_to_crop.crop((xmin,ymin,xmax,ymax))
        cropped_img.save(GImport.os.path.join(out_dir,fname))
