import Global_imports as GImport


def myOCR(img,out_prcssimg,rotation,txtfile):
    f = open(txtfile+".txt", "w+")

    print('file',img)
    img=GImport.cv2.imread(img)

    # Convert to gray
    img = GImport.cv2.cvtColor(img, GImport.cv2.COLOR_BGR2GRAY)

    # Apply dilation and erosion to remove some noise
    kernel = GImport.np.ones((1, 1), GImport.np.uint8)
    img = GImport.cv2.dilate(img, kernel, iterations=1)
    img = GImport.cv2.erode(img, kernel, iterations=1)

    rotated90 = GImport.imutils.rotate_bound(img, rotation)
    img = rotated90

    proccessedimg=out_prcssimg+str(txtfile)+'_'+"removed_noise.png"
    # Write image after removed noise
    GImport.cv2.imwrite(proccessedimg, img)

    imgtoStr = GImport.pytesseract.image_to_string(GImport.Image.open(proccessedimg),lang='eng',config='--psm 6 -c tessedit_char_whitelist=[A-Z]')
    for i in imgtoStr.split():
        print(i)
        # f.write(str("Profiles"))
        f.write(str(i))
        f.write(str('\n'))
    #text
    print('text',imgtoStr)




def myOCRforSlices(img,out_prcssimg,rotation):
    #print('file',img)
    fname = GImport.os.path.splitext(img)[0]
    filename = fname.split('/', 2)

    img=GImport.cv2.imread(img)

    im_shape = img.shape
    if im_shape[0] >= im_shape[1]:
        rotated90 = GImport.imutils.rotate_bound(img, 270)
        img = rotated90

    # Convert to gray
    img = GImport.cv2.cvtColor(img, GImport.cv2.COLOR_BGR2GRAY)

    # Apply dilation and erosion to remove some noise
    kernel = GImport.np.ones((1, 1), GImport.np.uint8)
    img = GImport.cv2.dilate(img, kernel, iterations=1)
    img = GImport.cv2.erode(img, kernel, iterations=1)

    rotated90 = GImport.imutils.rotate_bound(img, rotation)
    img = rotated90
    filesplit=filename[2].split('/',3)
    proccessedimg=out_prcssimg+filesplit[2]+'_'+"removed_noise.png"

    # Write image after removed noise
    GImport.cv2.imwrite(proccessedimg, img)
    imginfo = {}
    imginfo['Co-ordinates'] = str(filename[2])
    imgtoStr = GImport.pytesseract.image_to_string(GImport.Image.open(proccessedimg),lang='eng',config='--psm 6 -c tessedit_char_whitelist=[A-Z]')
    imginfo['Text']=imgtoStr

    #print('text',imginfo)
    return imginfo
