import Global_imports as GImport

def thresholding(gray_first_half_4x):
    gray_first_half_4x = GImport.cv2.cvtColor(gray_first_half_4x, GImport.cv2.COLOR_BGR2GRAY)
    threshold_first_half_4x = GImport.cv2.adaptiveThreshold(gray_first_half_4x, 255, GImport.cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                                    GImport.cv2.THRESH_BINARY, 101, 25)
    threshold_first_half_4x = GImport.cv2.erode(
        threshold_first_half_4x, None, iterations=1)
    return threshold_first_half_4x


def imageEnhance(id, fileName):
    inputdir = r'./static/ICDS/'+id+'/input/'
    path = fileName
    inpfile = fileName.split('.', 2)[0]
    print('path', path)

    img = GImport.cv2.imread(inputdir+path)
    resized_gray_125x = GImport.cv2.resize(
        img, None, fx=1.25, fy=1.25, interpolation=GImport.cv2.INTER_CUBIC)
    resized_gray_125x = thresholding(resized_gray_125x)
    enhance125xOut = inputdir + '125x_' + inpfile+'.png'
    GImport.cv2.imwrite(enhance125xOut, resized_gray_125x)
    return enhance125xOut

