import Global_imports as GImport

def myOCR(img,out_prcssimg,rotation,txtfile):
    f=open(txtfile+'.txt','w+')
    print('file',img)
    img=GImport.cv2.imread(img)
    # Convert to gray
    img = GImport.cv2.cvtColor(img, GImport.cv2.COLOR_BGR2GRAY)
    # Apply dilation and erosion to remove some noise
    kernel = GImport.np.ones((1, 1), GImport.np.uint8)
    img = GImport.cv2.dilate(img, kernel, iterations=1)
    img = GImport.cv2.erode(img, kernel, iterations=1)
    rotated90 = GImport.imutils.rotate_bound(img, rotation)
    img = rotated90
    
    txtfname=txtfile.split('/',6)
    txtname1=txtfname[len(txtfname)-1]
    proccessedimg=out_prcssimg+'/'+str(txtname1)+'_'+"removed_noise.png"
    # Write image after removed noise
    GImport.cv2.imwrite(proccessedimg, img)
    imgtoStr = GImport.pytesseract.image_to_string(GImport.Image.open(proccessedimg),lang='eng',config='--psm 6 -c tessedit_char_whitelist=[A-Z]')
    for i in imgtoStr.split():
        # f.write(str("Profiles"))
        f.write(str(i))
        f.write(str('\n'))
    #text

def getGridManually(id,img,coordinates):
    manualgriddir = r'./static/ICDS/'+id+'/grid_bubble/manual_grid/'
    filsplit = img.split('/', 5)
    imgFileName = filsplit[len(filsplit)-1]
    imgFileNameExt = imgFileName.split('.', 2)[0]
    print('file name',  imgFileNameExt)
    inpimg= GImport.cv2.imread(img)
    print("image path",img)
    Gridpoints=eval(coordinates)
    px,py,pwidth,pheight=Gridpoints['x'],Gridpoints['y'],Gridpoints['width'],Gridpoints['height']
    ptx,pty,ptwidth,ptheight=int(px),int(py),int(pwidth),int(pheight)   
   
    manual_crop_img = inpimg[pty:pty+ptheight, ptx:ptx+ptwidth]
    manual_crp=manualgriddir + imgFileNameExt + '_manualgrid.png'
    print(manual_crp)
    GImport.cv2.imwrite(manual_crp, manual_crop_img)
    #End of crop manually

    #Get_Radius_Manually
    radius=getRadiusmanually(manual_crp)
    manul_detected=GImport.xedcircle.detect_grids_manualRadius(id,img,radius)
    return manul_detected


def getRadiusmanually(imge):
    inpRadiusImage = GImport.cv2.imread(imge)
    #print("getradiusmanually path",inpRadiusImage)
    inpRadiusgray = GImport.cv2.cvtColor(inpRadiusImage,GImport.cv2.COLOR_BGR2GRAY)
    inpRadiusBlur = GImport.cv2.medianBlur(inpRadiusgray,5)
    Getcircles = GImport.cv2.HoughCircles(inpRadiusBlur,GImport.cv2.HOUGH_GRADIENT,2,100,minRadius=0,maxRadius=250)

    #circles = np.uint16(np.around(Getcircles))
    for i in Getcircles[0,:]:
        GImport.cv2.circle(inpRadiusImage,(i[0],i[1]),i[2],(255,128,0),12)
        GImport.cv2.circle(inpRadiusImage,(i[0],i[1]),2,(0,255,0),3)
        print("The radius of the circle is ",i[2])
        return i[2]
