import Global_imports as GImport


def getCrosshairs(inputImage,coordinates,minPoint,maxPoint,direction,outputpath):
    image = GImport.cv2.imread(inputImage)
    image1 = image
    img_gray = GImport.cv2.cvtColor(image, GImport.cv2.COLOR_BGR2GRAY)
    toptempCenter, sidetempCenter=[],[]
    threshold = 0.7
    
    if direction=='y':
        top_template_path = r'./static/ICDS/crosshair_templates/top_templates/'
        files = GImport.os.listdir(top_template_path)
        print('int(maxPoint[y]),int(minPoint[y])',int(maxPoint[1]),int(minPoint[1]))
        for file in files:
            template= GImport.cv2.imread(top_template_path+file)
            top_template = GImport.cv2.cvtColor(template, GImport.cv2.COLOR_BGR2GRAY)
            w, h = top_template.shape[::-1]
            res = GImport.cv2.matchTemplate(img_gray, top_template, GImport.cv2.TM_CCOEFF_NORMED)
            loc = GImport.np.where(res >= threshold)
            for pt1 in zip(*loc[::-1]):
                pt2 = (pt1[0] + w, pt1[1] + h)
                pt3 = (int(pt1[0] + w / 2), int(pt1[1] + h / 2))
                for x_co in coordinates:
                    if int(x_co[0]-5) <= pt3[0] >= int(x_co[0]+5):
                        if int(minPoint[1])>= pt3[1] or pt3[1] >=int(maxPoint[1]):
                            GImport.cv2.rectangle(image, pt1, pt2, (0, 0, 255), 3)
                            GImport.cv2.circle(image, pt3, 10, (255, 0, 0), 5)
                            toptempCenter.append(pt3)
        
        GImport.cv2.imwrite(outputpath + '_top_temp.png', image)
            

    elif direction=='x':
        side_template_path = r'./static/ICDS/crosshair_templates/side_templates/'
        files = GImport.os.listdir(side_template_path)
        for file in files:
            template= GImport.cv2.imread(side_template_path+file)
            side_template = GImport.cv2.cvtColor(template, GImport.cv2.COLOR_BGR2GRAY)
            w, h = side_template.shape[::-1]
            res = GImport.cv2.matchTemplate(img_gray, side_template, GImport.cv2.TM_CCOEFF_NORMED)
            loc = GImport.np.where(res >= threshold)
            for pt1 in zip(*loc[::-1]):
                pt2 = (pt1[0] + w, pt1[1] + h)
                pt3 = (int(pt1[0] + w / 2), int(pt1[1] + h / 2))
                for y_co in coordinates:
                    if int(y_co[1]-5)<= pt3[1] >= int(y_co[1]+5):
                          if int(minPoint[0])>= pt3[0] or pt3[0] >=int(maxPoint[0   ]):
                            GImport.cv2.rectangle(image, pt1, pt2, (0, 0, 255), 3)
                            GImport.cv2.circle(image, pt3, 10, (255, 0, 0), 5)
                            sidetempCenter.append(pt3)
                
        #print('sidetempCenter',sidetempCenter)
        GImport.cv2.imwrite(outputpath + '_side_temp.png', image)

def markDimension():

     return ''       