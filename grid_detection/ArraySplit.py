import Global_imports as GImport

def splitArr(input_array, splitbase):
    # splitbase is based on either x(0) or y(1)

    val = input_array[0][splitbase]
    newArr = []
    SplittedArray = []

    for i in range(len(input_array)):
        val_top = int(val + 100)
        val_bottom = int(val - 100)
        y_co = int(input_array[i][splitbase])
        if i == int(len(input_array) - 1):
            if val_bottom <= y_co <= val_top:
                newArr.append(input_array[i])
            newArr1 = newArr.copy()
            if len(newArr1):
                SplittedArray.append(newArr1)
        elif val_bottom <= y_co <= val_top:
            newArr.append(input_array[i])
        else:
            newArr1 = newArr.copy()
            if len(newArr1):
                SplittedArray.append(newArr1)
            val = input_array[i][splitbase]
            newArr.clear()
            newArr.append(input_array[i])

    return SplittedArray


def Getunique(input_array, baseAxis):
    uniqueArr = []
    if baseAxis == 0:
        X0Axis = input_array[0][0]
        uniqueArr.append(input_array[0])
        for i in range(len(input_array)):
            x_co = input_array[i][0]
            XAxis_top = X0Axis + 50
            XAxis_bottom = X0Axis - 50
            if XAxis_bottom <= x_co <= XAxis_top:
                #uniqueArr.append(input_array[i])
                continue
            elif X0Axis == x_co:
                #uniqueArr.append(input_array[i])
                continue
            elif x_co >= XAxis_top:
                X0Axis = input_array[i][0]
                uniqueArr.append(input_array[i])
            else:
                X0Axis = input_array[i][0]
                #uniqueArr.append(input_array[i])
    elif baseAxis == 1:
        Y0Axis = input_array[0][1]
        uniqueArr.append(input_array[0])
        for i in range(len(input_array)):
            y_co = input_array[i][1]
            YAxis_top = Y0Axis + 20
            YAxis_bottom = Y0Axis - 20
            if YAxis_bottom <= y_co <= YAxis_top:
                # uniqueArr.append(input_array[i])
                continue
            elif y_co >= YAxis_top:
                Y0Axis = input_array[i][1]
                uniqueArr.append(input_array[i])
            else:
                Y0Axis = input_array[i][1]
                # uniqueArr.append(input_array[i])

    return uniqueArr

crpdirec = r'./croped_images/'

def crop_image(img,pt1,pt2,orient,j):

    startY=pt1[1]
    endY=pt2[1]
    startX =pt1[0]
    endX =pt2[0]
    h=endY-startY
    w=startX-endX
    print(h,w)
    #roi = np.zeros((h, w, 3), dtype=np.uint8)
    roi = img[startY:endY, startX:endX]
    print('type',roi)
    crop = GImport.Image.fromarray(roi, 'RGB')
    crop.save(crpdirec + orient + str(j) + '.jpg')
    crop.show()
    #print(roi)
    #cv2.imwrite(crpdirec + orient + str(j) + '.jpg', roi)
    return 0


final_dict = {}
text_line_arr=[]
def crop_coordinateJSON(x1,x2,y1,y2):
    text_line = {}
    text_line['x1'] = x1
    text_line['x2'] = x2
    text_line['y1'] = y1
    text_line['y2'] = y2
    text_line_arr.append(text_line)

def  craete_cropJSON(filename):
    final_dict['text lines']=text_line_arr
    # If the file name exists, write a JSON string into the file.
    if filename:
        # Writing JSON data
        with open(filename, 'w') as f:
            GImport.json.dump(final_dict, f)


def crop_east_ims(jfile, filename, out_dir):
    jfile = GImport.json.load(open(jfile))
    jcoords = jfile['text lines']
    in_dir = GImport.os.path.splitext(os.path.basename(filename))[0]  # r'C:\Users\Admin\Documents\Custom Office Templates\results\inp17'
    filename = filename  # r'C:\Users\Admin\Documents\Custom Office Templates\results\inp17\input_images.png'
    out_dir = out_dir  # r'C:\Users\Admin\Documents\Custom Office Templates\results\inp17\out'
    img_to_crop = GImport.Image.open(filename)

    for ind, coords in enumerate(jcoords):
        xcoords =  [coords['x1'], coords['x2']]
        ycoords = [ coords['y1'], coords['y2']]
        xmin = min(xcoords)
        ymin = min(ycoords)
        xmax = max(xcoords)
        ymax = max(ycoords)
        print(xcoords,ycoords,ind)
        fname = GImport.os.path.basename(in_dir) + '_' + str(ind) + '.png'
        try:
            cropped_img = img_to_crop.crop((xmin, ymin, xmax, ymax))
            cropped_img.save(os.path.join(out_dir, fname))
        except:
            print('error')
