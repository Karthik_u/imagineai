import Global_imports as GImport


def combinecoordinate(id, txtfile, coordinates, xlsname):
    xlsopdirec = r'./static/ICDS/'+id+'/grid_bubble/xlsoutput/'
    # create of file xlsx using workbook class
    workbook = GImport.xlsxwriter.Workbook(xlsopdirec+xlsname+'.xlsx')
    # By default worksheet names in the spreadsheet will be
    # Sheet1, Sheet2 etc., but we can also specify a name.
    worksheet = workbook.add_worksheet("Grid_Coordinates")
    # Some data we want to write to the worksheet.
    gridName = []
    f = open(txtfile, "r")
    listOfLines = f.readlines()
    f.close()

    for line in listOfLines:
        # print(line.strip())
        gridName.append(line.strip())

    gridwithcoordinate = [['Grid Name', 'x', 'y', 'r']]
    for i in range(len(coordinates)):
        # print('i', i)
        x, y, r = coordinates[i]
        grid = gridName[i]
        print(grid)
        data = [grid, int(x), int(y), int(r)]
        gridwithcoordinate.append(data)

    gridwithcoordinatetuple = tuple(gridwithcoordinate)
    # Start from the first cell. Rows and
    # columns are zero indexed.
    row = 0
    col = 0

    # Iterate over the data and write it out row by row.
    for gridName, x, y, r in (gridwithcoordinatetuple):
        worksheet.write(row, col, gridName)
        worksheet.write(row, col + 1, x)
        worksheet.write(row, col + 2, y)
        worksheet.write(row, col + 3, r)
        row += 1

    workbook.close()


def detect_grids(id,fil):
    idirec = r'./static/ICDS/'+id+'/input/'
    detecteddirec = r'./static/ICDS/'+id+'/grid_bubble/grid_detected_image/'
    gdirec = r'./static/ICDS/'+id+'/grid_bubble/'
    processeddirec = r'./static/ICDS/'+id+'/grid_bubble/processed/'
    croppeddirec = r'./static/ICDS/'+id+'/grid_bubble/croppedGrids/'
    ocrtxtdirec = r'./static/ICDS/'+id+'/grid_bubble/txtoutput/'
    tempOPdirec = r'./static/ICDS/'+id+'/grid_bubble/template/'

    filsplit = fil.split('/', 5)
    imgFileName = filsplit[len(filsplit)-1]
    imgFileNameExt = imgFileName.split('.', 2)[0]
    imgFileNameExt = imgFileNameExt[5:]
    print('file name', imgFileNameExt)

    #enhanceOut = GImport.Img_enhance.imageEnhance('demo01', imgFileName)

    Original_image = GImport.cv2.imread(fil)

    image = Original_image
    image_detected = Original_image.copy()
    gray = GImport.cv2.cvtColor(image,  GImport.cv2.COLOR_BGR2GRAY)
    blur = GImport.cv2.medianBlur(gray, 5)
    circles = GImport.cv2.HoughCircles(
        blur,  GImport.cv2.HOUGH_GRADIENT, 2, 100, minRadius=30, maxRadius=150)

    circlexy = []
    for i in circles[0, :]:
        # draw the outer circle
        if i[2] < 99:
            GImport.cv2.circle(image, (i[0], i[1]), i[2], (0, 128, 0), 15)
            GImport.cv2.circle(image_detected, (i[0], i[1]),
                               i[2], (255, 255, 255), 15)
            circlexy.append((i[0], i[1], i[2]))
           # points = i[1], i[0]    # points='x=',i[1],'y=',i[0]
        elif i[2] < 199:
            GImport.cv2.circle(image, (i[0], i[1]), i[2], (0, 255, 255), 13)
            GImport.cv2.circle(
                image_detected, (i[0], i[1]), i[2], (0, 255, 255), 15)
            circlexy.append((i[0], i[1], i[2]))

    # sort the based on x values
    circlexy.sort(key=GImport.operator.itemgetter(0))
    circlexy1 = circlexy.copy()
    circlexy1.reverse()

    # remove the another list and it will display the side array values only
    Side_Arr_X = GImport.ArraySplit.splitArr(circlexy, 0)[0]

    # sorted based on y cordinate value
    Side_Arr_X.sort(key=GImport.operator.itemgetter(1))

    circlexy1.sort(key=GImport.operator.itemgetter(1))
    Side_Arr_Y = GImport.ArraySplit.splitArr(circlexy1, 1)[0]
    Side_Arr_Y.sort(key=GImport.operator.itemgetter(0))

    side_mincir = Side_Arr_X[0]  # side array of first value
    # side array of the second value
    side_maxcir = Side_Arr_X[len(Side_Arr_X) - 1]

    top_mincir = Side_Arr_Y[0]
    top_maxcir = Side_Arr_Y[len(Side_Arr_Y)-1]

    sidepointA = (side_mincir[0] - side_mincir[2],
                  side_mincir[1] - side_mincir[2])
    sidepointB = (side_maxcir[0] + side_maxcir[2],
                  side_maxcir[1] + side_maxcir[2])
    toppointA = (top_mincir[0] - top_mincir[2], top_mincir[1] - top_mincir[2])
    toppointB = (top_maxcir[0] + top_maxcir[2], top_maxcir[1] + top_maxcir[2])

    GImport.cv2.rectangle(image, sidepointA, sidepointB, (255, 0, 0), 5)

    GImport.cv2.rectangle(image, toppointA, toppointB, (255, 0, 0), 5)

    GImport.cv2.imwrite(detecteddirec+imgFileNameExt+'_detected.jpg', image)

    sidecropped = image_detected[int(sidepointA[1]):int(sidepointA[1]) + int(sidepointB[1] - sidepointA[1]),
                                 int(sidepointA[0]):int(sidepointA[0]) + int(sidepointB[0] - sidepointA[0])]
    topcropped = image_detected[int(toppointA[1]):int(toppointA[1]) + int(toppointB[1] - toppointA[1]),
                                int(toppointA[0]):int(toppointA[0]) + int(toppointB[0] - toppointA[0])]

    top_crop = croppeddirec + imgFileNameExt + '_top_crop.png'
    side_crop = croppeddirec + imgFileNameExt + '_side_crop.png'
    GImport.cv2.imwrite(top_crop, topcropped)
    GImport.cv2.imwrite(side_crop, sidecropped)

    GImport.OCR_tess.myOCR(top_crop, processeddirec, 0,
                           ocrtxtdirec+imgFileNameExt+'_top_crop')

    GImport.OCR_tess.myOCR(side_crop, processeddirec, 0,
                           ocrtxtdirec+imgFileNameExt+'_side_crop')

    combinecoordinate('demo01', ocrtxtdirec+imgFileNameExt +
                      '_top_crop.txt', Side_Arr_Y, imgFileNameExt+'_top_Grid_Xls')

    combinecoordinate('demo01', ocrtxtdirec+imgFileNameExt +
                      '_side_crop.txt', Side_Arr_X, imgFileNameExt+'_side_Grid_Xls')

    # GImport.Get_Crosshairs.getCrosshairs(enhanceOut, Side_Arr_Y, min(Side_Arr_X), max(
    #     Side_Arr_X), 'y', tempOPdirec+imgFileNameExt)  # top

    # GImport.Get_Crosshairs.getCrosshairs(enhanceOut, Side_Arr_X, min(Side_Arr_Y), max(
    #     Side_Arr_Y), 'x', tempOPdirec+imgFileNameExt)  # side

    return detecteddirec+imgFileNameExt+'_detected.jpg'


def detect_grids_manualRadius( id,fil, radius):
    idirec = r'./static/ICDS/'+id+'/input/'
    detecteddirec = r'./static/ICDS/'+id+'/grid_bubble/grid_detected_image/'
    gdirec = r'./static/ICDS/'+id+'/grid_bubble/'
    processeddirec = r'./static/ICDS/'+id+'/grid_bubble/processed/'
    croppeddirec = r'./static/ICDS/'+id+'/grid_bubble/croppedGrids/'
    ocrtxtdirec = r'./static/ICDS/'+id+'/grid_bubble/txtoutput/'
    tempOPdirec = r'./static/ICDS/'+id+'/grid_bubble/template/'

    filsplit = fil.split('/', 5)
    imgFileName = filsplit[len(filsplit)-1]
    imgFileNameExt = imgFileName.split('.', 2)[0]
    imgFileNameExt = imgFileNameExt[5:]
    print('file name', imgFileNameExt)

    #enhanceOut = GImport.Img_enhance.imageEnhance('demo01', imgFileName)

    Original_image = GImport.cv2.imread(fil)

    image = Original_image
    image_detected = Original_image.copy()
    gridMinRadius = int(radius-10)
    gridMaxRadius = int(radius+10)
    print('gridMaxRadius,gridMinRadius', gridMaxRadius, gridMinRadius)
    gray = GImport.cv2.cvtColor(image,  GImport.cv2.COLOR_BGR2GRAY)
    blur = GImport.cv2.medianBlur(gray, 5)
    circles = GImport.cv2.HoughCircles(
        blur,  GImport.cv2.HOUGH_GRADIENT, 2, 100, minRadius=0, maxRadius=250)

    print('circles', circles)
    circlexy = []
    for i in circles[0, :]:
        if gridMinRadius >= i[2] <= gridMaxRadius:
            GImport.cv2.circle(image, (i[0], i[1]), i[2], (0, 128, 0), 15)
            GImport.cv2.circle(image_detected, (i[0], i[1]),
                               i[2], (255, 255, 255), 15)
            circlexy.append((i[0], i[1], i[2]))

    # sort the based on x values
    circlexy.sort(key=GImport.operator.itemgetter(0))
    circlexy1 = circlexy.copy()
    circlexy1.reverse()

    # remove the another list and it will display the side array values only
    Side_Arr_X = GImport.ArraySplit.splitArr(circlexy, 0)[0]

    # sorted based on y cordinate value
    Side_Arr_X.sort(key=GImport.operator.itemgetter(1))

    circlexy1.sort(key=GImport.operator.itemgetter(1))
    Side_Arr_Y = GImport.ArraySplit.splitArr(circlexy1, 1)[0]
    Side_Arr_Y.sort(key=GImport.operator.itemgetter(0))

    side_mincir = Side_Arr_X[0]  # side array of first value
    # side array of the second value
    side_maxcir = Side_Arr_X[len(Side_Arr_X) - 1]

    top_mincir = Side_Arr_Y[0]
    top_maxcir = Side_Arr_Y[len(Side_Arr_Y)-1]

    sidepointA = (side_mincir[0] - side_mincir[2],
                  side_mincir[1] - side_mincir[2])
    sidepointB = (side_maxcir[0] + side_maxcir[2],
                  side_maxcir[1] + side_maxcir[2])
    toppointA = (top_mincir[0] - top_mincir[2], top_mincir[1] - top_mincir[2])
    toppointB = (top_maxcir[0] + top_maxcir[2], top_maxcir[1] + top_maxcir[2])

    GImport.cv2.rectangle(image, sidepointA, sidepointB, (255, 0, 0), 5)

    GImport.cv2.rectangle(image, toppointA, toppointB, (255, 0, 0), 5)

    GImport.cv2.imwrite(detecteddirec+imgFileNameExt+'_detected.jpg', image)

    sidecropped = image_detected[int(sidepointA[1]):int(sidepointA[1]) + int(sidepointB[1] - sidepointA[1]),
                                 int(sidepointA[0]):int(sidepointA[0]) + int(sidepointB[0] - sidepointA[0])]
    topcropped = image_detected[int(toppointA[1]):int(toppointA[1]) + int(toppointB[1] - toppointA[1]),
                                int(toppointA[0]):int(toppointA[0]) + int(toppointB[0] - toppointA[0])]

    top_crop = croppeddirec + imgFileNameExt + '_top_crop.png'
    side_crop = croppeddirec + imgFileNameExt + '_side_crop.png'
    GImport.cv2.imwrite(top_crop, topcropped)
    GImport.cv2.imwrite(side_crop, sidecropped)

    GImport.OCR_tess.myOCR(top_crop, processeddirec, 0,
                           ocrtxtdirec+imgFileNameExt+'_top_crop')

    GImport.OCR_tess.myOCR(side_crop, processeddirec, 0,
                           ocrtxtdirec+imgFileNameExt+'_side_crop')

    combinecoordinate('demo01', ocrtxtdirec+imgFileNameExt +
                      '_top_crop.txt', Side_Arr_Y, imgFileNameExt+'_top_Grid_Xls')

    combinecoordinate('demo01', ocrtxtdirec+imgFileNameExt +
                      '_side_crop.txt', Side_Arr_X, imgFileNameExt+'_side_Grid_Xls')

    # GImport.Get_Crosshairs.getCrosshairs(enhanceOut, Side_Arr_Y, min(Side_Arr_X), max(
    #     Side_Arr_X), 'y', tempOPdirec+imgFileNameExt)  # top

    # GImport.Get_Crosshairs.getCrosshairs(enhanceOut, Side_Arr_X, min(Side_Arr_Y), max(
    #     Side_Arr_Y), 'x', tempOPdirec+imgFileNameExt)  # side

    return detecteddirec+imgFileNameExt+'_detected.jpg'
